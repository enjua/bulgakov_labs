package SWERC_2017;

import java.util.Scanner;

public class task_F{
	static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args){
		int width = scanner.nextInt();
		int numberOfPieces = scanner.nextInt();
		int[] cakes = new int[numberOfPieces];
		for(int i = 0; i < numberOfPieces; i++){
			cakes[i] = scanner.nextInt()*scanner.nextInt();
		}
		long summAr=0;
		for(int i = 0; i < numberOfPieces; i++){
			summAr+=cakes[i];
		}
		System.out.println(summAr/width);
	}
}
