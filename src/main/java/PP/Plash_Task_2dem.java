package PP;

import java.util.Arrays;

public class Plash_Task_2dem{

	public static void main(String[] args){

				if(args.length < 3){
					System.out.println(0);
					return;
				}

		int rainBlock = 0;

		int leftWall;
		int rightWall;

		int[] wallsHeight = new int[args.length];

		for(int i = 0; i < args.length; i++){
			wallsHeight[i] = Integer.parseInt(args[i]);
		}

		int maxHeight = Arrays.stream(wallsHeight).max().getAsInt();

		for(int i = maxHeight; i > 0; i--){

			leftWall = -1;
			rightWall = -1;


			for(int j = 0; j < wallsHeight.length; j++){

				if(wallsHeight[j]>=i){

					if(leftWall==-1){
						leftWall=j;
						continue;
					}
					if(leftWall!=-1){

						if(rightWall==-1){
							rightWall=j;
						}

						if(hasFirstLayer(wallsHeight,leftWall,rightWall)){
							rainBlock += rightWall - leftWall - 1;
						}
						rightWall=-1;leftWall=-1;
						continue;
					}
				}
				else{continue;}
			}
		}
		System.out.println(rainBlock);
	}

	static boolean hasFirstLayer(int[] wallHeight, int start, int end){
		for(int i = start; i < end; i++){
			if(wallHeight[i]==0){return false;}
		}
		return true;
	}
}
