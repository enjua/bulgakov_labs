package PP.PK;

import java.util.Scanner;

//http://codeforces.com/problemset/problem/114/A
public class task5{
	private static final Scanner scan = new Scanner(System.in);

	public static void main(String[] args){
		int number = scan.nextInt();
		int pow = scan.nextInt();
		int result = 0;
		while(pow>number){
			pow/=number;
			result++;
		}

		if(number!=pow){
			System.out.println("NO");
		}
		else{
			System.out.println("YES");
			System.out.println(result);
		}
	}
}
