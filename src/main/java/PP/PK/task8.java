package PP.PK;

import java.util.Scanner;

//http://codeforces.com/problemset/problem/127/B
public class task8{
	public static final Scanner scan = new Scanner(System.in);

	public static void main(String[] args){

		int[] count = new int[100];
		int result = 0;

		int qty = scan.nextInt();

		for(int i = 0; i < qty; i++){
			count[scan.nextInt()]+=1;
		}

		for(int i = 0; i < count.length; i++){
			result+=count[i]/2;
		}
		result/=2;
		System.out.println(result);

	}

}
