package PP.PK;

import java.util.Scanner;

//http://codeforces.com/problemset/problem/112/A
public class task1{
	private static final Scanner scan = new Scanner(System.in);

	public static void main(String[] args){
		String str1 = scan.nextLine();
		String str2 = scan.nextLine();
		int result = str1.toLowerCase().compareTo(str2.toLowerCase());
		result = Integer.compare(result, 0);
		System.out.println(result);
	}

}
