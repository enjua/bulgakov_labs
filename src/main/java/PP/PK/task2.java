package PP.PK;

import java.util.Scanner;

//http://codeforces.com/problemset/problem/102/B

public class task2{
	public static final Scanner scan = new Scanner(System.in);

	public static void main(String[] args){
		int number=scan.nextInt();
		int count = 0;
		while(getDigitsSum(number)!=number){
			number=getDigitsSum(number);
			count++;
		}
		System.out.println(count);


	}
	public static int getDigitsSum(int number){
		int result = 0;
		if(number<10){return number;}
		String arr = String.valueOf(number);
		for(int i = 0; i < arr.length(); i++){
			result+=Integer.parseInt(String.valueOf(arr.charAt(i)));
		}
		return result;
	}

}
