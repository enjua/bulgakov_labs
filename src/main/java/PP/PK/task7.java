package PP.PK;

import java.util.Scanner;

//http://codeforces.com/problemset/problem/129/A

public class task7{
	public static final Scanner scan = new Scanner(System.in);

	public static void main(String[] args){
		int[] cookie = new int[scan.nextInt()];
		int cookieNumber = 0;
		int tmp;
		int result = 0;
		for(int i = 0; i < cookie.length; i++){
			tmp=scan.nextInt();
			cookieNumber+=tmp;
			cookie[i]=tmp;
		}
		for(int i = 0; i < cookie.length; i++){
			if(((cookieNumber-cookie[i])&1)==0){
				result++;
			}
		}
		System.out.println(result);

	}

}
