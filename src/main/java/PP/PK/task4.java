package PP.PK;

import java.util.Scanner;

//http://codeforces.com/problemset/problem/104/A
public class task4{

	private static Scanner scan = new Scanner(System.in);

	public static void main(String[] args){
		int currentScore = 10;
		int needScore = scan.nextInt();
		if(currentScore == needScore || needScore - currentScore > 11 || needScore < currentScore){
			System.out.println(0);
			return;
		} else if(needScore - currentScore == 10){
			System.out.println(15);
			return;
		} else{
			System.out.println(4);
		}

	}
}
