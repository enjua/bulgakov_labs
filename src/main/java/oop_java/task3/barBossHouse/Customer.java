package oop_java.task3.barBossHouse;

import java.util.Objects;

public final class Customer {

    private static final String DEFAULT_FIRST_NAME = "";
    private static final String DEFAULT_SECOND_NAME = "";
    private static final int DEFAULT_AGE = -1;
    //todo: ты ебанутый?
    public final Customer MATURE_UNKNOWN_CUSTOMER = new Customer(21);
    public final Customer NOT_MATURE_UNKNOWN_CUSTOMER = new Customer(14);
    private final String fistName;
    private final String secondName;
    private final int age;
    private final Address address;

    public Customer() {
        this(DEFAULT_AGE);
    }

    public Customer(int age) {
        this(DEFAULT_FIRST_NAME, DEFAULT_SECOND_NAME, age, Address.EMPTY_ADDRESS);
    }

    public Customer(String fistName, String secondName, int age, Address address) {
        this.fistName = fistName;
        this.secondName = secondName;
        this.age = age;
        this.address = address;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Customer)) {
            return false;
        }
        Customer customer = (Customer) o;
        return age == customer.age && fistName.equals(customer.fistName) && secondName.equals(customer.secondName) && address.equals(customer.address);
    }

    @Override
    public int hashCode() {
//todo: здесь захотел класс юзать, а в остальных не помешало ксорить?)
        return Objects.hash(fistName, secondName, age, address);
    }

}
