package oop_java.task3.barBossHouse;

public class InternetOrder implements Order {

    private int size;
    private ListNode first;
    private ListNode last;
    private Customer customer;

    public InternetOrder() {
        this.first = this.last = new ListNode();
        this.size = 0;
    }

    public InternetOrder(MenuItem[] items, Customer customer) {
        this.customer = customer;
        this.size = 0;
        this.first = this.last = new ListNode();
        for (int i = 0; i < items.length; i++) {
            if (items[i] != null) {
                this.last.value = items[i];
                this.last.next = new ListNode();
                this.last = this.last.next;
                this.size++;
            }
        }
    }

    //todo: ну блин дублируется же код, вынес бы в отдельный класс
    private static void quickSort(MenuItem[] arr, int low, int high) {

        if (arr == null || arr.length == 0) {
            return;
        }
        if (low >= high) {
            return;
        }
        int middle = low + (high - low) / 2;
        MenuItem pivot = arr[middle];
        int i = low, j = high;
        while (i <= j) {
            while (arr[i].getCost() < pivot.getCost()) {
                i++;
            }
            while (arr[j].getCost() > pivot.getCost()) {
                j--;
            }
            if (i <= j) {
                swap(arr, i, j);
                i++;
                j--;
            }
        }
        if (low < j) {
            quickSort(arr, low, j);
        }
        if (high > i) {
            quickSort(arr, i, high);
        }
    }

    public static void swap(MenuItem array[], int x, int y) {
        MenuItem temp = array[x];
        array[x] = array[y];
        array[y] = temp;
    }

    public boolean add(MenuItem item) {
        if (this.last.value == null) {
            //			this.last.value = item;
            //			this.last.next = new ListNode();
            this.last = new ListNode(item);
            this.last = this.last.next;
        } else {
            this.last.next = new ListNode(item);
            this.last = this.last.next;
        }
        size++;
        return true;
    }
    //todo: да, три ту сайз)
    //trim to size?
    public String[] itemNames() {
        String[] names = new String[this.size];
        int current = 0;
        ListNode currentElement = this.first;
        do {
            if (!this.contains(names, currentElement.value.getName())) {
                names[current++] = currentElement.value.getName();
            }
            currentElement = currentElement.next;
        }
        while (currentElement.next != null);
        return itemNames();
    }

    private boolean contains(String[] array, String name) {
        for (int i = 0; i < array.length; i++) {
            //todo: руку отрежу! © Азазелло
            if (array[i] == name) {
                return true;
            }
        }
        return false;
    }

    public int itemsQuantity() {
        return size;
    }

    public int itemQuantity(String itemName) {
        int quantity = 0;
        ListNode current = this.first;
        while (current.value != null) {
            if (current.value.getName().equals(itemName)) {
                quantity++;
            }
            current = current.next;
        }
        return quantity;
    }

    public int itemQuantity(MenuItem item) {
        int quantity = 0;
        ListNode current = this.first;
        while (current.value != null) {
            if (current.value.equals(item)) {
                quantity++;
            }
            current = current.next;
        }
        return quantity;
    }

    public MenuItem[] getItems() {
        MenuItem[] items = new MenuItem[size];
        int currentNumb = 0;
        ListNode current = this.first;
        while (current.value != null) {
            items[currentNumb++] = current.value;
            current = current.next;
        }
        return items;
    }

    public boolean remove(String itemName) {
        if (first.value.getName().equals(itemName)) {
            first = first.next;
            this.size--;
            return true;
        } else {
            ListNode current = first.next;
            ListNode prev = last;
            while (current.value != null) {
                if (current.value.getName().equals(itemName)) {
                    prev.next = current.next;
                    this.size--;
                    return true;
                } else {
                    current = current.next;
                    prev = prev.next;
                }
            }
        }
        return false;
    }

    public boolean remove(MenuItem item) {
        if (first.value.equals(item)) {
            first = first.next;
            this.size--;
            return true;
        } else {
            ListNode current = first.next;
            ListNode prev = last;
            while (current.value != null) {
                if (current.value.equals(item)) {
                    prev.next = current.next;
                    this.size--;
                    return true;
                } else {
                    current = current.next;
                    prev = prev.next;
                }
            }
        }
        return false;
    }

    public int removeAll(String itemName) {
        int quantity = 0;
        if (first.value.getName().equals(itemName)) {
            first = first.next;
            this.size--;
            quantity++;
        }
        ListNode current = first.next;
        ListNode prev = last;
        while (current.value != null) {
            if (current.value.getName().equals(itemName)) {
                prev.next = current.next;
                this.size--;
                quantity++;
            } else {
                current = current.next;
                prev = prev.next;
            }
        }
        return quantity;
    }

    public int removeAll(MenuItem item) {
        int quantity = 0;
        if (first.value.equals(item)) {
            first = first.next;
            this.size--;
            quantity++;
        }
        ListNode current = first.next;
        ListNode prev = last;
        while (current.value != null) {
            if (current.value.equals(item)) {
                prev.next = current.next;
                this.size--;
                quantity++;
            } else {
                current = current.next;
                prev = prev.next;
            }
        }
        return quantity;
    }

    public MenuItem[] sortedItemsByCostDesc() {
        MenuItem[] items = this.getItems();
        quickSort(items, 0, items.length - 1);
        return items;
    }

    public double costTotal() {
        ListNode current = this.first;
        double totalCost = 0;
        while (current.next != null) {
            totalCost += current.value.getCost();
            current = current.next;
        }
        totalCost += current.value.getCost();
        return totalCost;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("InternetOrder:");
        builder.append("\n" + customer.toString());
        ListNode current = this.first;
        while (current.next != null) {
            builder.append(current.value.toString());
        }
        builder.append(current.value.toString());
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof InternetOrder))
            return false;
        InternetOrder that = (InternetOrder) o;
        return that.customer.equals(this.customer) && (that.size == this.size) && this.compareCustomerList(this.first, that.first);
    }
    //todo: лолшто
    private boolean compareCustomerList(ListNode o1, ListNode o2) {
        if (o1 == null || o2 == null) return true;
        do {
            if (o1.value != o2.value) {
                return false;
            }
        }
        while (o1.next != null);
        return true;
    }

    @Override
    public int hashCode() {
        return ((Integer) size).hashCode() ^ customer.hashCode() ^ listHashCode(first);
    }

    private int listHashCode(ListNode headOfList) {
        int result = 31;
        ListNode current = headOfList;
        do {
            result ^= 31 * current.value.hashCode();
            current = current.next;
        }
        while (current.next != null);
        return result;
    }

    class ListNode {

        ListNode next;
        MenuItem value;

        ListNode() {
            this(null);
        }

        ListNode(MenuItem value) {
            this.value = value;
            this.next = null;
        }
    }
}
