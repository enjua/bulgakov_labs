package oop_java.task3.barBossHouse;

public interface Alcoholable {

    boolean isAlcoholicDrink();

    double getAlcoholVol();
}
