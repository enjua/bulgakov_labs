package oop_java.task3.barBossHouse;

public class InternetOrdersManager implements OrdersManager {
    //todo: приватными поля кто будет делать?
    QueueNode head;
    QueueNode tail;
    int size;

    public InternetOrdersManager() {
        head = tail = new QueueNode();
        this.size = 0;
    }

    public InternetOrdersManager(Order[] orders) {
        this.head = new QueueNode();
        QueueNode current = head;
        int quantity = 0;
        for (int i = 0; i < orders.length; i++) {
            current.value = orders[i];
            current = current.next = new QueueNode();
            quantity++;
        }
        tail = current;
        this.size = quantity;
    }

    public boolean add(Order order) {
        tail = tail.next = new QueueNode(order);
        this.size++;
        return true;
    }

    public Order peek() {
        return head.value;
    }

    public Order pop() {
        Order result = head.value;
        head = head.next;
        this.size--;
        return result;
    }

    public int ordersQuantity() {
        return size;
    }

    public double ordersCostSummary() {
        double summaryCost = 0;
        QueueNode current = head;
        do {
            summaryCost += current.value.costTotal();
            current = current.next;
        }
        while (current.next != null);
        return summaryCost;
    }

    public int itemsQuantity(String itemName) {
        int itemsNumber = 0;
        QueueNode current = head;
        do {
            itemsNumber += current.value.itemQuantity(itemName);
            current = current.next;
        }
        while (current.next != null);
        return itemsNumber;
    }

    public int itemsQuantity(MenuItem item) {
        int itemsNumber = 0;
        QueueNode current = head;
        do {
            itemsNumber += current.value.itemQuantity(item);
            current = current.next;
        }
        while (current.next != null);
        return itemsNumber;
    }

    public Order[] getOrders() {
        Order[] orders = new Order[this.size];
        QueueNode current = head;
        for (int i = 0; i < orders.length; i++) {
            orders[i] = current.value;
            current = current.next;
        }
        return orders;
    }

    class QueueNode {

        QueueNode next;
        //todo: а почему нот юзед?
        QueueNode prev;
        Order value;

        public QueueNode() {
        }

        public QueueNode(Order value) {
            this.value = value;
        }

    }
}
