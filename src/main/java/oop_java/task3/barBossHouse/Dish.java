package oop_java.task3.barBossHouse;

public class Dish extends MenuItem {

    public Dish(String name, String description) {
        super(name, description);
    }

    public Dish(double cost, String name, String description) {
        super(cost, name, description);
    }

    @Override
    public String toString() {
        return "Dish: " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Dish) && super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode() ^ this.getClass().getCanonicalName().hashCode();
    }

    @Override
    public Dish clone() {
        return new Dish(this.getCost(), this.getName(), this.getDescription());
    }
}