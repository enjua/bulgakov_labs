package oop_java.task3.barBossHouse;

public interface Order {

    boolean add(MenuItem item);

    String[] itemNames();

    int itemsQuantity();

    int itemQuantity(String itemName);

    int itemQuantity(MenuItem item);

    MenuItem[] getItems();

    boolean remove(String itemName);

    boolean remove(MenuItem item);

    int removeAll(String itemName);

    int removeAll(MenuItem item);

    MenuItem[] sortedItemsByCostDesc();

    double costTotal();

    Customer getCustomer();

    void setCustomer(Customer customer);
}
