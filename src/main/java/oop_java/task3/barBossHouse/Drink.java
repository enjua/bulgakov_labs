package oop_java.task3.barBossHouse;

import java.util.Objects;

public final class Drink extends MenuItem implements Alcoholable {

    private static final double DEFAULT_ALCOHOL_VOLUME = 0;
    private static final String DEFAULT_DESCRIPTION = "";
    private final double alcoholVol;
    private final DrinkTypeEnum type;

    public Drink(String name, DrinkTypeEnum type) {
        this(DEFAULT_COST, name, DEFAULT_DESCRIPTION, DEFAULT_ALCOHOL_VOLUME, type);
    }

    //todo: Ну вообще помещать кост в начало не есть хорошо-то
    public Drink(double cost, String name, String description, DrinkTypeEnum type) {
        this(cost, name, DEFAULT_DESCRIPTION, DEFAULT_ALCOHOL_VOLUME, type);
    }
    //todo: что-то ты не тот конструктор вызываешь
    public Drink(double cost, String name, String description, double alcoholVol, DrinkTypeEnum type) {
        super(cost, name, description);
        this.alcoholVol = alcoholVol;
        this.type = type;
    }

    public DrinkTypeEnum getType() {
        return type;
    }

    @Override
    public boolean isAlcoholicDrink() {
        return this.alcoholVol > 0;
    }

    @Override
    public double getAlcoholVol() {
        return this.alcoholVol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Drink))
            return false;
        Drink drink = (Drink) o;
        return Double.compare(drink.alcoholVol, alcoholVol) == 0 && type == drink.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), alcoholVol, type) ^ this.getClass().getCanonicalName().hashCode();
    }

    @Override
    public String toString() {
        return "Drink: " + type + super.toString();
    }

    @Override
    public MenuItem clone() {
        return new Drink(this.getCost(), this.getName(), this.getDescription(), this.alcoholVol, this.type);
    }
}
