package oop_java.task3.barBossHouse;

import java.util.Objects;

public abstract class MenuItem {

    protected static final int DEFAULT_COST = 0;
    private double cost;
    private String name;
    private String description;

    protected MenuItem(String name, String description) {
        this(DEFAULT_COST, name, description);
    }

    protected MenuItem(double cost, String name, String description) {
        this.cost = cost;
        this.name = name;
        this.description = description;
    }

    public double getCost() {
        return cost;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof MenuItem))
            return false;
        MenuItem menuItem = (MenuItem) o;
        return Double.compare(menuItem.cost, cost) == 0 && name.equals(menuItem.name) && description.equals(menuItem.description);
    }

    @Override
    public int hashCode() {

        return Objects.hash(cost, name, description);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(name).append(", ");
        if (this.cost != 0) {
            builder.append(cost).append("$. ");
        }
        builder.append(description);
        return builder.toString();
    }

    public abstract MenuItem clone();
}
