package oop_java.task3.barBossHouse;

import java.util.Arrays;

public class TableOrder implements Order {

    private static final int DEFAULT_SIZE = 16;
    private Customer customer;
    private int size;
    private MenuItem[] menuItems;

    public TableOrder() {
        this(DEFAULT_SIZE, null);
    }

    public TableOrder(int size, Customer customer) {
        //        this(new MenuItem[size]);
        this.size = size;
        this.menuItems = new MenuItem[size];
        this.customer = customer;
    }

    public TableOrder(MenuItem[] menuItems, Customer customer) {
        this.menuItems = menuItems.clone();
        this.customer = customer;
        for (int i = 0; i < menuItems.length; i++) {
            if (this.menuItems[i] != null) {
                size++;
            }
        }
    }

    private static void quickSort(MenuItem[] arr, int low, int high) {

        if (arr == null || arr.length == 0) {
            return;
        }
        if (low >= high) {
            return;
        }
        int middle = low + (high - low) / 2;
        MenuItem pivot = arr[middle];
        int i = low, j = high;
        while (i <= j) {
            while (arr[i].getCost() < pivot.getCost()) {
                i++;
            }
            while (arr[j].getCost() > pivot.getCost()) {
                j--;
            }
            if (i <= j) {
                swap(arr, i, j);
                i++;
                j--;
            }
        }
        if (low < j) {
            quickSort(arr, low, j);
        }
        if (high > i) {
            quickSort(arr, i, high);
        }
    }

    public static void swap(MenuItem array[], int x, int y) {
        MenuItem temp = array[x];
        array[x] = array[y];
        array[y] = temp;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public boolean add(MenuItem menuItem) {
        if (this.menuItems.length <= size + 1) {
            increaseOrderLength();
        }
        for (int i = 0; i < this.menuItems.length; i++) {
            if (this.menuItems[i] == null) {
                this.menuItems[i] = menuItem.clone();
            }
        }
        return true;
    }

    private void increaseOrderLength() {
        MenuItem[] newArr = new MenuItem[this.menuItems.length * 2];
        System.arraycopy(this.menuItems, 0, newArr, 0, this.menuItems.length);
        this.menuItems = newArr;
    }

    public boolean remove(String dishName) {
        for (int i = 0; i < this.menuItems.length; i++) {
            if (this.menuItems[i] != null && this.menuItems[i].getName().equals(dishName)) {
                System.arraycopy(this.menuItems, i + 1, this.menuItems, i, this.menuItems.length - i);
                return true;
            }
        }
        return false;
    }

    public boolean remove(MenuItem item) {
        for (int i = 0; i < this.menuItems.length; i++) {
            if (this.menuItems[i] != null && this.menuItems[i].equals(item)) {
                System.arraycopy(this.menuItems, i + 1, this.menuItems, i, this.menuItems.length - i);
                return true;
            }
        }
        return false;
    }

    public int removeAll(String dishName) {
        int removedQuantity = 0;
        for (int i = 0; i < this.menuItems.length; i++) {
            if (this.menuItems != null && this.menuItems[i].getName().equals(dishName)) {
                this.menuItems[i] = null;
                removedQuantity++;
            }
        }
        return removedQuantity;
    }

    public int removeAll(MenuItem item) {
        int removedQuantity = 0;
        for (int i = 0; i < this.menuItems.length; i++) {
            if (this.menuItems != null && this.menuItems[i].equals(item)) {
                this.menuItems[i] = null;
                removedQuantity++;
            }
        }
        return removedQuantity;
    }

    public int itemsQuantity() {
        return this.size;
    }

    public int itemQuantity(String dishName) {
        int dishQuantity = 0;
        for (int i = 0; i < this.menuItems.length; i++) {
            if (this.menuItems[i] != null && this.menuItems[i].getName().equals(dishName)) {
                dishQuantity++;
            }
        }
        return dishQuantity;
    }

    public int itemQuantity(MenuItem item) {
        int dishQuantity = 0;
        for (int i = 0; i < this.menuItems.length; i++) {
            if (this.menuItems[i] != null && this.menuItems[i].equals(item)) {
                dishQuantity++;
            }
        }
        return dishQuantity;
    }

    public MenuItem[] getItems() {
        MenuItem[] menuItems = new MenuItem[this.size];
        int current = 0;
        for (int i = 0; i < this.menuItems.length; i++) {
            if (this.menuItems[i] != null) {
                menuItems[current++] = this.menuItems[i];
            }
        }
        return menuItems;
    }

    public double costTotal() {
        double totalCost = 0;
        for (int i = 0; i < this.menuItems.length; i++) {
            if (this.menuItems[i] != null) {
                totalCost += this.menuItems[i].getCost();
            }
        }
        return totalCost;
    }
    //todo: йеп
    //return names array without empty elements?
    public String[] itemNames() {
        String[] names = new String[size];
        int current = 0;
        for (int i = 0; i < this.size; i++) {
            if (!contains(names, this.menuItems[i].getName())) {
                names[current++] = this.menuItems[i].getName();
            }
        }
        return names;
    }

    private boolean contains(String[] arr, String val) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != null && arr[i].equals(val)) {
                return true;
            }
        }
        return false;
    }

    public MenuItem[] sortedItemsByCostDesc() {
        MenuItem[] items = this.getItems();
        quickSort(items, 0, items.length - 1);
        return items;
    }

    //Simple clone() realisation. Will be changed later.
    @Override
    public TableOrder clone() {
        return new TableOrder(this.menuItems, this.customer);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("TableOrder: " + this.size);
        for (int i = 0; i < this.menuItems.length; i++) {
            if (this.menuItems[i] != null) {
                builder.append(this.menuItems[i].toString());
            }
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof TableOrder))
            return false;
        TableOrder tableOrder = (TableOrder) o;
        return size == tableOrder.size && customer.equals(tableOrder.customer) && Arrays.equals(menuItems, tableOrder.menuItems);
    }

    @Override
    public int hashCode() {
        int result = 31;
        result ^= customer.hashCode();
        result ^= ((Integer) size).hashCode();
        result ^= Arrays.hashCode(menuItems);
        return result;
    }
}
