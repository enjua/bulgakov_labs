package oop_java.task2.barBossHouse;

public class OrderManager {

    private Order[] orders;

    public OrderManager(int qtyOfTable) {
        orders = new Order[qtyOfTable];
    }


    public boolean add(Order order, int tableNumber) {
        this.orders[tableNumber] = order.clone();
        return true;
    }

    public Order getOrder(int tableNumber) {
        return this.orders[tableNumber];
    }

    public void addDish(Dish dish, int tableNumber) {
        this.orders[tableNumber].add(dish);
    }

    public void removeOrder(int tableNumber) {
        this.orders[tableNumber] = null;
    }

    public int freeTableNumber() {
        for (int i = 0; i < orders.length; i++) {
            if (this.orders[i] == null) {
                return i;
            }
        }
        return -1;
    }

    public int[] freeTableNumbers() {
        int[] tableNumbers = new int[freeTableQuantity()];
        int current = 0;
        for (int i = 0; i < this.orders.length; i++) {
            if (this.orders[i] == null) {
                tableNumbers[current++] = i;
            }
        }
        return tableNumbers;
    }

    public int[] occupiedTableNumbers() {
        int[] tableNumbers = new int[freeTableQuantity()];
        int current = 0;
        for (int i = 0; i < this.orders.length; i++) {
            if (this.orders[i] != null) {
                tableNumbers[current++] = i;
            }
        }
        return tableNumbers;
    }

    private int freeTableQuantity() {
        int quantity = 0;
        for (int i = 0; i < this.orders.length; i++) {
            if (this.orders[i] == null) {
                quantity++;
            }
        }
        return quantity;
    }

    private int occupiedTableQuantity() {
        return this.orders.length - freeTableQuantity();
    }

    public Order[] getOrders() {
        Order[] tableOrders = new Order[occupiedTableQuantity()];
        int current = 0;
        for (int i = 0; i < this.orders.length; i++) {
            if (this.orders[i] != null) {
                tableOrders[current++] = this.orders[i];
            }
        }
        return tableOrders;
    }

    public double ordersCostSummary() {
        double cost = 0;
        for (int i = 0; i < this.orders.length; i++) {
            if (this.orders[i] != null) {
                cost += this.orders[i].costTotal();
            }
        }
        return cost;
    }

    public int dishQuantity(String dishName) {
        int qty = 0;
        for (int i = 0; i < this.orders.length; i++) {
            qty += this.orders[i].dishQuantity(dishName);
        }
        return -1;
    }
}
