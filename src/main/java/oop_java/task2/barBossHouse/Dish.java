package oop_java.task2.barBossHouse;

public class Dish {

    private static final int DEFAULT_COST = 0;
    private double cost;
    private String name;
    private String description;

    public Dish(String name, String description) {
        this(DEFAULT_COST, name, description);
    }

    public Dish(double cost, String name, String description) {
        this.cost = cost;
        this.name = name;
        this.description = description;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(name).append(" ").append(cost).append("$. ").append(description);
        return builder.toString();
    }

    //Simple clone() realisation. Will be changed later.
    @Override
    public Dish clone() {
        return new Dish(this.cost, this.name, this.description);
    }
}
