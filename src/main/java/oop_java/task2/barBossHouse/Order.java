package oop_java.task2.barBossHouse;

public class Order {

    private static final int DEFAULT_SIZE = 16;
    private int size;
    private Dish[] dishes;

    public Order() {
        this(DEFAULT_SIZE);
    }

    public Order(int size) {
        //        this(new Dish[size]);
        this.size = size;
        this.dishes = new Dish[size];
    }

    public Order(Dish[] dishes) {
        this.dishes = dishes.clone();
        for (int i = 0; i < dishes.length; i++) {
            if (this.dishes[i] != null) {
                size++;
            }
        }
    }

    private static void quickSort(Dish[] arr, int low, int high) {

        if (arr == null || arr.length == 0) {
            return;
        }

        if (low >= high) {
            return;
        }

        int middle = low + (high - low) / 2;
        Dish pivot = arr[middle];

        int i = low, j = high;
        while (i <= j) {
            while (arr[i].getCost() < pivot.getCost()) {
                i++;
            }
            while (arr[j].getCost() > pivot.getCost()) {
                j--;
            }
            if (i <= j) {
                swap(arr, i, j);
                i++;
                j--;
            }
        }
        if (low < j) {
            quickSort(arr, low, j);
        }
        if (high > i) {
            quickSort(arr, i, high);
        }
    }

    public static void swap(Dish array[], int x, int y) {
        Dish temp = array[x];
        array[x] = array[y];
        array[y] = temp;
    }

    public boolean add(Dish dish) {
        if (this.dishes.length <= size + 1) {
            increaseOrderLength();
        }
        for (int i = 0; i < this.dishes.length; i++) {
            if (this.dishes[i] == null) {
                this.dishes[i] = dish.clone();
            }
        }
        return true;
    }

    private void increaseOrderLength() {
        Dish[] newArr = new Dish[this.dishes.length * 2];
        System.arraycopy(this.dishes, 0, newArr, 0, this.dishes.length);
        this.dishes = newArr;
    }

    public boolean remove(String dishName) {
        for (int i = 0; i < this.dishes.length; i++) {
            if (this.dishes[i] != null && this.dishes[i].getName().equals(dishName)) {
                System.arraycopy(this.dishes, i + 1, this.dishes, i, this.dishes.length - i);
                return true;
            }
        }
        return false;
    }

    public int removeAll(String dishName) {
        int removedQuantity = 0;
        for (int i = 0; i < this.dishes.length; i++) {
            if (this.dishes != null && this.dishes[i].getName().equals(dishName)) {
                this.dishes[i] = null;
                removedQuantity++;
            }
        }
        return removedQuantity;
    }

    public int dishQuantity() {
        return this.size;
    }

    public int dishQuantity(String dishName) {
        int dishQuantity = 0;
        for (int i = 0; i < this.dishes.length; i++) {
            if (this.dishes[i] != null && this.dishes[i].getName().equals(dishName)) {
                dishQuantity++;
            }
        }
        return dishQuantity;
    }

    public Dish[] getDishes() {
        Dish[] dishes = new Dish[this.size];
        int current = 0;
        for (int i = 0; i < this.dishes.length; i++) {
            if (this.dishes[i] != null) {
                dishes[current++] = this.dishes[i];
            }
        }
        return dishes;
    }

    public double costTotal() {
        double totalCost = 0;
        for (int i = 0; i < this.dishes.length; i++) {
            if (this.dishes[i] != null) {
                totalCost += this.dishes[i].getCost();
            }
        }
        return totalCost;
    }

    //return names array without empty elements?
    public String[] dishesNames() {
        String[] names = new String[size];
        int current = 0;
        for (int i = 0; i < this.size; i++) {
            if (!contains(names, this.dishes[i].getName())) {
                names[current++] = this.dishes[i].getName();
            }
        }
        return names;
    }

    private boolean contains(String[] arr, String val) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != null && arr[i].equals(val)) {
                return true;
            }
        }
        return false;
    }

    //TODO babushkin sort
    public Dish[] sortedDishesByCostDesc() {
        Dish[] result = this.dishes.clone();
        quickSort(result, 0, result.length - 1);
        return result;
    }

    //Simple clone() realisation. Will be changed later.
    @Override
    public Order clone() {
        return new Order(this.dishes.clone());
    }
}