package oop_java.task4.barBossHouse.barBossHouse;

public enum DrinkTypeEnum {
    BEER,
    WINE,
    VODKA,
    BRANDY,
    CHAMPAGNE,
    WHISKEY,
    TEQUILA,
    RUM,
    VERMUTH,
    LIQUOR,
    JAGERMAISTER,
    JUICE,
    COFEE,
    GREEN_TEA,
    BLACK_TEA,
    MILK,
    WATER,
    CARBONATED_WATER
}