package oop_java.task4.barBossHouse.barBossHouse;

import java.util.Objects;

public final class Address {

    public static final Address EMPTY_ADDRESS = new Address();
    private static final String DEFAULT_CITY_NAME = "Samara";
    private static final int DEFAULT_ZIP_CODE = -1;
    private static final String DEFAULT_STREET_NAME = "";
    private static final int DEFAULT_BUILDING_NUMBER = -1;
    private static final char DEFAULT_BUILDING_LETTER = ' ';
    private static final int DEFAULT_APARTMENT_NUMBER = -1;
    private final String cityName;
    private final int zipCode;
    private final String streetName;
    private final int buildingNumber;
    private final char buildingLetter;
    private final int apartmentNumber;

    public Address() {
        this(DEFAULT_STREET_NAME, DEFAULT_BUILDING_NUMBER, DEFAULT_BUILDING_LETTER, DEFAULT_APARTMENT_NUMBER);
    }

    public Address(String streetName, int buildingNumber, char buildingLetter, int apartmentNumber) {
        this(DEFAULT_CITY_NAME, DEFAULT_ZIP_CODE, streetName, buildingNumber, buildingLetter, apartmentNumber);
    }

    public Address(String cityName, int zipCode, String streetName, int buildingNumber, char buildingLetter, int apartmentNumber) {
        if(zipCode<0|buildingNumber<0|apartmentNumber<0|!Character.isLetter(buildingLetter)){
            throw new IllegalArgumentException();
        }
        this.cityName = cityName;
        this.zipCode = zipCode;
        this.streetName = streetName;
        this.buildingNumber = buildingNumber;
        this.buildingLetter = buildingLetter;
        this.apartmentNumber = apartmentNumber;
    }

    public String getCityName() {
        return cityName;
    }

    public int getZipCode() {
        return zipCode;
    }

    public String getStreetName() {
        return streetName;
    }

    public int getBuildingNumber() {
        return buildingNumber;
    }

    public char getBuildingLetter() {
        return buildingLetter;
    }

    public int getApartmentNumber() {
        return apartmentNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Address)) {
            return false;
        }
        Address address = (Address) o;
        return zipCode == address.zipCode && buildingNumber == address.buildingNumber && buildingLetter == address.buildingLetter && apartmentNumber == address.apartmentNumber && cityName.equals(address.cityName) && streetName.equals(address.streetName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cityName, zipCode, streetName, buildingNumber, buildingLetter, apartmentNumber);
    }

}