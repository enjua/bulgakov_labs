package oop_java.task4.barBossHouse.barBossHouse;

import java.util.Objects;

public final class Drink extends MenuItem implements Alcoholable {

    private static final double DEFAULT_ALCOHOL_VOLUME = 0;
    private static final String DEFAULT_DESCRIPTION = "";
    private final double alcoholVol;
    private final DrinkTypeEnum type;

    public Drink(String name, DrinkTypeEnum type) {
        this(name, DEFAULT_DESCRIPTION, DEFAULT_ALCOHOL_VOLUME, type,DEFAULT_COST);
    }

    //Ну вообще помещать кост в начало не есть хорошо-то
    //fixed
    public Drink(String name, String description, DrinkTypeEnum type,double cost) {
        this(name, DEFAULT_DESCRIPTION, DEFAULT_ALCOHOL_VOLUME, type,cost);
    }
    //что-то ты не тот конструктор вызываешь
    //вроде все гут. вызываю констурктор супер класса чтобы заполнить поля которые есть в нем,
    //и далее заполняю оставшиеся поля
    public Drink(String name, String description, double alcoholVol, DrinkTypeEnum type,double cost) {
        super(name, description,cost);
        if(alcoholVol<0|alcoholVol>100){
            {
                throw new IllegalArgumentException();
            }
        }
        this.alcoholVol = alcoholVol;
        this.type = type;
    }

    public DrinkTypeEnum getType() {
        return type;
    }

    @Override
    public boolean isAlcoholicDrink() {
        return this.alcoholVol > 0;
    }

    @Override
    public double getAlcoholVol() {
        return this.alcoholVol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Drink))
            return false;
        Drink drink = (Drink) o;
        return Double.compare(drink.alcoholVol, alcoholVol) == 0 && type == drink.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), alcoholVol, type) ^ this.getClass().getCanonicalName().hashCode();
    }

    @Override
    public String toString() {
        return "Drink: " + type + super.toString();
    }

    @Override
    public MenuItem clone() {
        return new Drink(this.getName(), this.getDescription(), this.alcoholVol, this.type,this.getCost());
    }
}
