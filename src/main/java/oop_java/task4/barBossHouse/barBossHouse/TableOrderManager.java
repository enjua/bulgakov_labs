package oop_java.task4.barBossHouse.barBossHouse;

public class TableOrderManager implements OrdersManager {

    private TableOrder[] tableOrders;

    public TableOrderManager(int qtyOfTable) {
        tableOrders = new TableOrder[qtyOfTable];
    }


    public boolean add(TableOrder tableOrder, int tableNumber) {
        this.tableOrders[tableNumber] = tableOrder.clone();
        return true;
    }

    public TableOrder getOrder(int tableNumber) {
        return this.tableOrders[tableNumber];
    }

    public void addMenuItem(MenuItem menuItem, int tableNumber) {
        this.tableOrders[tableNumber].add(menuItem);
    }

    public void removeOrder(int tableNumber) {
        this.tableOrders[tableNumber] = null;
    }

    public int removeOrder(Order order) {
        for (int i = 0; i < this.tableOrders.length; i++) {
            if (this.tableOrders[i].equals(order)) {
                this.tableOrders[i] = null;
                return i;
            }
        }
        return -1;
    }

    public int removeAllOrder(Order order) {
        int qty = -1;
        for (int i = 0; i < this.tableOrders.length; i++) {
            if (this.tableOrders[i].equals(order)) {
                this.tableOrders[i] = null;
                qty++;
            }
        }
        return qty;
    }

    public int freeTableNumber() {
        for (int i = 0; i < tableOrders.length; i++) {
            if (this.tableOrders[i] == null) {
                return i;
            }
        }
        return -1;
    }
    //todo а давай ты эти методы все же перепишешь на предикат
    public int[] freeTableNumbers() {
        int[] tableNumbers = new int[freeTableQuantity()];
        int current = 0;
        for (int i = 0; i < this.tableOrders.length; i++) {
            if (this.tableOrders[i] == null) {
                tableNumbers[current++] = i;
            }
        }
        return tableNumbers;
    }

    public int[] occupiedTableNumbers() {
        int[] tableNumbers = new int[this.tableOrders.length-freeTableQuantity()];
        int current = 0;
        for (int i = 0; i < this.tableOrders.length; i++) {
            if (this.tableOrders[i] != null) {
                tableNumbers[current++] = i;
            }
        }
        return tableNumbers;
    }

    private int freeTableQuantity() {
        int quantity = 0;
        for (int i = 0; i < this.tableOrders.length; i++) {
            if (this.tableOrders[i] == null) {
                quantity++;
            }
        }
        return quantity;
    }

    private int occupiedTableQuantity() {
        return this.tableOrders.length - freeTableQuantity();
    }

    public TableOrder[] getTableOrders() {
        TableOrder[] tableTableOrders = new TableOrder[occupiedTableQuantity()];
        int current = 0;
        for (int i = 0; i < this.tableOrders.length; i++) {
            if (this.tableOrders[i] != null) {
                tableTableOrders[current++] = this.tableOrders[i];
            }
        }
        return tableTableOrders;
    }

    public double ordersCostSummary() {
        double cost = 0;
        for (int i = 0; i < this.tableOrders.length; i++) {
            if (this.tableOrders[i] != null) {
                cost += this.tableOrders[i].costTotal();
            }
        }
        return cost;
    }
    //fixed давай я тебе жнк буду звать?
    public int itemQuantity(String itemName) {
        int quantity = 0;
        for (int i = 0; i < this.tableOrders.length; i++) {
            quantity += this.tableOrders[i].itemQuantity(itemName);
        }
        return quantity;
    }


    public int ordersQuantity() {
        int qty = 0;
        for (TableOrder order : this.tableOrders) {
            qty++;
        }
        return qty;
    }

    public int itemsQuantity(MenuItem item) {
        int qty = 0;
        for (TableOrder order : this.tableOrders) {
            for (MenuItem menuItem : order.getItems()) {
                if (menuItem.equals(item)) {
                    qty++;
                }
            }
        }
        return qty;
    }

    public int itemsQuantity(String itemName) {
        int qty = 0;
        for (TableOrder order : this.tableOrders) {
            for (MenuItem menuItem : order.getItems()) {
                if (menuItem.equals(itemName)) {
                    qty++;
                }
            }
        }
        return qty;
    }

    public Order[] getOrders() {
        return this.tableOrders;
    }
}
