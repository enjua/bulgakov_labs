package oop_java.task4.barBossHouse.barBossHouse;

import java.time.LocalDateTime;

public interface OrdersManager {

    int itemsQuantity(String itemName);

    int itemsQuantity(MenuItem item);

    Order[] getOrders();

    int numberOfOrderInDay(LocalDateTime time);

    Order[] ordersInDay(LocalDateTime time);

    Order[] ordersOfClient(Customer customer);

    double ordersCostSummary();

    int ordersQuantity();
}
