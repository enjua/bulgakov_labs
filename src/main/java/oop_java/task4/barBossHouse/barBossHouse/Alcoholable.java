package oop_java.task4.barBossHouse.barBossHouse;

public interface Alcoholable {

    boolean isAlcoholicDrink();

    double getAlcoholVol();
}
