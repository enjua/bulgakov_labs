package oop_java.task4.barBossHouse.barBossHouse;

import java.time.LocalDateTime;
import java.util.Objects;

public final class Customer {

    private static final String DEFAULT_FIRST_NAME = "";
    private static final String DEFAULT_SECOND_NAME = "";

    private static final LocalDateTime DEFAULT_TIME = LocalDateTime.now();
    private final String fistName;
    private final String secondName;
    private final LocalDateTime birthDate;
    private final Address address;

    public Customer() {
        this(DEFAULT_TIME);
    }

    public Customer(LocalDateTime time) {
        this(DEFAULT_FIRST_NAME, DEFAULT_SECOND_NAME, time, Address.EMPTY_ADDRESS);
    }

    public Customer(String fistName, String secondName, LocalDateTime birthDate, Address address) {
        if(birthDate.isAfter(LocalDateTime.now())) {
            throw new IllegalArgumentException();
        }
        this.fistName = fistName;
        this.secondName = secondName;
        this.birthDate = birthDate;
        this.address = address;

    }

    public int getAge(){
        return LocalDateTime.now().getYear()-this.birthDate.getYear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Customer)) {
            return false;
        }
        Customer customer = (Customer) o;
        return customer.equals(this.birthDate) && fistName.equals(customer.fistName) && secondName.equals(customer.secondName) && address.equals(customer.address);
    }

    @Override
    public int hashCode() {
        return fistName.hashCode()^secondName.hashCode()^ this.birthDate.hashCode() ^address.hashCode();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Customer:");
        sb.append(fistName).append(' ');
        sb.append(secondName).append(' ');
        sb.append(birthDate);
        sb.append(", address=").append(address);
        return sb.toString();
    }

}
